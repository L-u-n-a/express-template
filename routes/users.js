/*jshint esversion: 6*/
var express = require('express');
var router = express.Router();

const {mongoose} = require('./../bin/db/mongoose');
const {user} = require('./../bin/db/models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  let newUser = new user({
    email: 'test@test.nl'
  });

  newUser.save().then((doc) => {
    user.find().then((users) => {
      res.render('users', {title: 'Users', users: users});
    }, (e) => {
      res.status(400).send();
    });
  }, (e) => {
    res.status(400).send();
  });
});


module.exports = router;
