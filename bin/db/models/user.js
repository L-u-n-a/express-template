/*jshint esversion: 6*/
const mongoose = require('mongoose');

const user = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 5,
  }
});

module.exports = {user};
